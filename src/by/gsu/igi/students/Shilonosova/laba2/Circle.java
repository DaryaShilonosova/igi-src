package by.gsu.igi.students.Shilonosova.laba2;

import java.util.Scanner;

public class Circle {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Введите радиус окружности: ");
        double radius = in.nextDouble();
        System.out.println("Radius=" + radius);
        double pi = 3.1416;
        double perimeter = (radius + radius) * pi;
        System.out.println("Perimeter=" + perimeter);
        double area = (radius * radius) * pi;
        System.out.println("area=" + area);
    }
}
